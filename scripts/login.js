/**
 * Created by Administrator on 2016/6/24.
 */

$(function () {
    //todo
   /* $.getJSON("http://192.168.0.102:8080/system/member/test",
        function(json){
           alert(json);
    });*/

    $("#btnLogin").click(function () {
        var icnum=$("#icnum").val();
        var password=$("#password").val();
        if(icnum==''){
            weui.alert("请填写会员号");
            return false;
        }
        if(password==''){
            weui.alert("请填写密码");
            return false;
        }
        var formDataArray=$("#loginFrm").serializeArray();
        console.log(formDataArray);
        $.ajax({
            url: server+"/system/member/login",
            type: "POST",
            dataType:'json',
            data: formDataArray,
            timeout: 50000,
            beforeSend : function(){
                //jsonp 方式此方法不被触发.原因可能是dataType如果指定为jsonp的话,就已经不是ajax事件了
                weui.Loading.show('处理中');
            },
            success: function (json) {//客户端jquery预先定义好的callback函数,成功获取跨域服务器上的json数据后,会动态执行这个callback函数
                if(json.bizData.success){
                    weui.Loading.hide();
                    console.log(json.bizData.bizData);
                    JSON.stringify(json.bizData.bizData);
                    appStorage.setItem("usertoken",JSON.stringify(json.bizData.bizData));
                    window.location.href="index.html";
                }else{
                    weui.alert(json.bizData.msg);
                }
            },
            complete : function(XMLHttpRequest, textStatus){
                weui.Loading.hide();
            },
            error: function(xhr){
                console.log("测试");
                //jsonp 方式此方法不被触发.原因可能是dataType如果指定为jsonp的话,就已经不是ajax事件了
                //请求出错处理
                window.location.href="login.html";
                weui.alert("请求出错(请检查相关度网络状况.)");
            }
        });
    });
    $("#btnreg").click(function () {
        $("#btnLogin").toggleClass("blackbgbtn");
        $("#btnreg").addClass("blackbgbtn");
    });

});