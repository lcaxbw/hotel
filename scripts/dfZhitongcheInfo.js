/**
 * Created by Administrator on 2016/6/26.
 */
var arrDate ;
var depDate;
var number ;
$(function () {

    //todo
    /* $.getJSON("http://192.168.0.102:8080/system/member/test",
     function(json){
     alert(json);
     });*/
 //   $("#arrDate").val(getDateStr(0))
 //   $("#depDate").val(getDateStr(1))
 //   $("#number").val(1)
 //   arrDate = $("#arrDate").val();
//    depDate = $("#depDate").val();
 //   number  = $("#number").val();

    $("#btnSearch").click(function () {
        //检查表单
        if($("#frmSearch input[name='arrDate']").val()==''){
            weui.alert("请填写入住日期");
            return false;
        }
        if($("#frmSearch input[name='depDate']").val()==''){
            weui.alert("请填写离开日期");
            return false;
        }


        var formDataArray=$("#frmSearch").serializeArray();
        console.log(formDataArray);
        weui.Loading.show('处理中');
        $.ajax({
            url: server+"/smit/queryRooms",
            type: "POST",
            data:formDataArray,
            //     timeout: 3000,
            beforeSend: function(){
                //jsonp 方式此方法不被触发.原因可能是dataType如果指定为jsonp的话,就已经不是ajax事件了
                weui.Loading.show('处理中');
            },
            success: function (json) {//客户端jquery预先定义好的callback函数,成功获取跨域服务器上的json数据后,会动态执行这个callback函数
                console.log(json);
                if (json.bizData.success){
                    $(".fangxing ul").html("");
                    var rows=json.bizData.bizData.data;
                    for (var i=0;i<rows.length;i++){
                        console.log(rows[i]);
                        var hotelRoomVO=rows[i];
                        var html = " <a>"+
                            "<li class='aui-list-view-cell aui-img'>"+
                            "<price class='aui-pull-right'><i>¥</i>"+hotelRoomVO.price_min+" <em class='emClick' roomname='"+hotelRoomVO.roomName+"' roomcode='"+hotelRoomVO.roomCode+"'></em></price>"+
                            "<img class='aui-img-object aui-pull-left' src='"+hotelRoomVO.roomPic+"'>"+
                            "<div class='aui-img-body'>"+
                            "<span class='title'>"+hotelRoomVO.roomName+"</span>"+
                            "<p class='aui-ellipsis-1'><span> </span><span> </span><span> </span></p>"+
                            "</div>"+
                            "</li>"+
                            "<ol class='aui-list-view priceList' id='"+hotelRoomVO.roomCode+"' style='display: none;'></ol>";

                        $(".fangxing ul").append(html);
                        bindEm();
                    }
                }else{
                    weui.alert(json.bizData.msg);
                }
            },
            complete: function(XMLHttpRequest, textStatus){
                weui.Loading.hide();
            },
            error: function(xhr){
                //jsonp 方式此方法不被触发.原因可能是dataType如果指定为jsonp的话,就已经不是ajax事件了
                //请求出错处理
                weui.alert("请求出错(请检查相关度网络状况.)");
            }
        });
    });

});

function bindEm(){
    $(".emClick").css("background-image","url(images/dingfang.png)")
    $(".emClick").css("background-size","cover")


    $(".emClick").unbind("click");
    $(".emClick").bind("click",function () {
        var  roomcode = $(this).attr("roomcode");
        var  roomname = $(this).attr("roomname");

        if($("#"+roomcode).is(":hidden")){
            queryPriceInfo(roomcode,roomname);

        }
        $("#"+roomcode).toggle(200);
    })


}

function queryPriceInfo(roomCode,roomname){
    roomname=encodeURI(roomname);
    roomname=encodeURI(roomname);
    var vo={arrDate:arrDate,depDate:depDate,number:number,roomCode:roomCode} ;
    $.ajax({
        async:false,
        url: server+"/smit/queryPriceInfo",
        type: "POST",
        data:vo,
        timeout: 5000,
        beforeSend: function(){
            //jsonp 方式此方法不被触发.原因可能是dataType如果指定为jsonp的话,就已经不是ajax事件了
        },
        success: function (json) {//客户端jquery预先定义好的callback函数,成功获取跨域服务器上的json数据后,会动态执行这个callback函数
            console.log(json);
            if (json.bizData!=null){
                $("#"+roomCode).html("");
                var rows=json.bizData.bizData.data;
                for (var i=0;i<rows.length;i++){
                    console.log(rows[i]);
                    var hotelRoomVO=rows[i];
                    var href ='dfZhitongche.html?priceCode='+hotelRoomVO.priceCode+'&roomCode='+roomCode+"&arrDate="+arrDate+"&depDate="+depDate+"&priceDetail="+hotelRoomVO.priceDetail+"&number="+number+"&roomName="+roomname;
                    var selectRoom = {priceCode:hotelRoomVO.priceCode,roomCode:roomCode};

                    var html =
                        "<a href='"+href+"'>"+
                        " <li class='aui-list-view-cell aui-img'>"+
                        "<price class='aui-pull-right'><i>¥</i>"+hotelRoomVO.priceDetail+"</price>"+
                        "<div class='aui-img-body'>"+
                        " <span class='title'>"+hotelRoomVO.priceName+"</span><b> </b>"+
                        "<p class='aui-ellipsis-1'><span> </span><span> </span><span> </span></p>"+
                        "</div>"+
                        "</li>"+
                        "</a>";
                    $("#"+roomCode).append(html);
                    bindEm();
                }
            }
        },
        complete: function(XMLHttpRequest, textStatus){
        },
        error: function(xhr){
            //jsonp 方式此方法不被触发.原因可能是dataType如果指定为jsonp的话,就已经不是ajax事件了
            //请求出错处理
            weui.alert("请求出错(请检查相关度网络状况.)");
        }
    });
}

function getDateStr(AddDayCount) {
    var dd = new Date();
    dd.setDate(dd.getDate()+AddDayCount);//获取AddDayCount天后的日期
    var y = dd.getFullYear();
    var m = dd.getMonth()+1;//获取当前月份的日期
    var d = dd.getDate();
    return y+"/"+m+"/"+d;
}
