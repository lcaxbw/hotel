/**
 * Created by Administrator on 2016/7/17.
 */
$(function () {
    if(appStorage.getItem("usertoken")==undefined){
        window.location.href="login.html";
    }
    else{
        $("#div_noLogin").hide();
        var userinfo = JSON.parse(appStorage.getItem('usertoken'));
        $("#div_okLogin span").get(0).innerText=userinfo.name;
        $("#div_okLogin span").get(1).innerText=userinfo.desc;
        $("#zhanghaoinfo input[name='name']").val(userinfo.name);
        //取得性别
        if(userinfo.sex)
        {
            var sexVal = userinfo.sex;

            if($("#sex").get(0).options[0].value == sexVal)
            {
                $("#sex").get(0).options[0].selected = true;
            }
            else
            {
                $("#sex").get(0).options[1].selected = true;
            }
        }
        var certfy
        $("#zhanghaoinfo input[name='phone']").val(userinfo.phone);
        $("#zhanghaoinfo input[name='birthday']").val(userinfo.birthday);

        $("#div_okLogin").show();

        //省市区
        //初始化省市区下拉框
        initComplexArea('seachprov', 'seachcity', 'seachdistrict', area_array, sub_array, '0', '0', '0');
        if(userinfo.nation) //选中用户已设置数据
        {
            var area = userinfo.nation.split(",");
            if (area[0]) {
                setValueOfArea("seachprov", area[0]);
                if (area[1]) {
                    setValueOfArea("seachcity", area[1]);
                    if (area[2]) {
                        if(area[2] == "请选择")
                        {
                            $("#seachdistrict_div").hide()
                        }
                        else
                        {
                            setValueOfArea("seachdistrict", area[2]);
                        }
                        if(area[3]) {
                            $("#"+"address").val([area[3]]);
                        }
                    }
                }
            }
        }

        //取得证件类型
        if(userinfo.crtftyp)
        {
            var type = userinfo.crtftyp;
            $("#crtftyp").get(0).options[type].selected = true;
            if(userinfo.crtfnum)
            {
                $("#"+"crtfnum").val(userinfo.crtfnum);
            }
        }
        //取得电子邮箱
        if(userinfo.email)
        {
            $("#"+"email").val(userinfo.email);
        }
    }

    $("#saveZh").click(function () {
        var name = $("#zhanghaoinfo input[name='name']").val();
        var sex =  $("#sex option:selected").val();
        var birthday = $("#zhanghaoinfo input[name='birthday']").val();
        var nation  = $("#seachprov option:selected").text() + "," + $("#seachcity option:selected").text() + ","
                     + $("#seachdistrict option:selected").text() + "," + $("#address").val();
        var crtftyp =  $("#crtftyp option:selected").text();
        var crtfnum = $("#crtfnum").val();
        var phone = $("#"+"phone").val();
        var email = $("#"+"email").val();

        //校验姓名
        if(!name)
        {
            weui.alert("请输入姓名");
            return false;
        }

        //校验出生日期
        if(!birthday)
        {
            weui.alert("请输入出生日期");
            return false;
        }

        //校验省市区，都不能为空
        var selectProv = $("#seachprov option:selected").val();
        //if( selectProv==11 || selectProv==12 || selectProv==31 || selectProv==50 || selectProv==71 || selectProv==81 || selectProv==82)
        //{
        //    nation = $("#seachprov option:selected").text() + "," + $("#seachcity option:selected").text() + ","+ $("#address").val();
        //}
        //else
        //{
        //    nation = $("#seachprov option:selected").text() + "," + $("#seachcity option:selected").text() + ","
        //    + $("#seachdistrict option:selected").text() + "," + $("#address").val();
        //}
        if( selectProv==11 || selectProv==12 || selectProv==31 || selectProv==50 || selectProv==71 || selectProv==81 || selectProv==82)
        {
            if($("#seachcity option:selected").val()==0 || $("#address").val()=="")
            {
                weui.alert("请输入完整地址");
                return false;
            }
        }
        else if($("#seachprov option:selected").val()==0 || $("#seachcity option:selected").val()==0 || $("#seachdistrict option:selected").val()==0 || $("#address").val()=="")
        {
            weui.alert("请输入完整地址");
            return false;
        }

        //校验证件号码
        var isIDCard1 = /^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/;
        if(crtftyp=="身份证" && crtfnum){
             if(!isIDCard1.test(crtfnum)){
                weui.alert("请输入正确证件号码");
                return false;
            }
        }
        else
        {
            if(!crtfnum)
            {
                weui.alert("请输入证件号码");
                return false;            }
        }

        //校验邮箱格式
        if(!email)
        {
            weui.alert("请输入邮箱");
            return false;
        }
        var jsonData =
            "{" +
                "editType:" + "2" + "," +
                "icnum:" + userinfo.icnum + "," +
                "name:" + name + "," +
                "phone:" + phone + "," +
                "email:" + email + "," +
                "nation:" + nation + "," +
                "crtftyp:" + crtftyp + "," +
                "crtfnum:" + crtfnum + "," +
                "birthday:" + birthday + "," +
                "sex:" + sex +
            "}";
        var obj={"editType":2,"icnum":userinfo.icnum,"name":name,"phone":phone,"email":email,"nation":nation,"crtftyp":crtftyp,"crtfnum":crtfnum,"birthday":birthday,"sex":sex};
        console.log(jsonData);
        $.ajax({
            url: server+"/system/member/userUpdate",
            type: "POST",
            dataType:'json',
            data: obj,
            timeout: 50000,
            beforeSend: function(){
                //jsonp 方式此方法不被触发.原因可能是dataType如果指定为jsonp的话,就已经不是ajax事件了
                weui.Loading.show('处理中');
            },
            success: function (json) {//客户端jquery预先定义好的callback函数,成功获取跨域服务器上的json数据后,会动态执行这个callback函数
            },
            complete: function(XMLHttpRequest, textStatus){
                weui.Loading.hide();
            },
            error: function(xhr){
                //jsonp 方式此方法不被触发.原因可能是dataType如果指定为jsonp的话,就已经不是ajax事件了
                //请求出错处理
                weui.alert("请求出错(请检查相关度网络状况.)");
            }
        });
    });
});

//省市区选择相关
//根据用户设置显示选中的省市区
function setValueOfArea(selectId, selectValue)
{
    var count = $("#"+selectId).get(0).options.length;
    for(var i=0; i<count; i++){
        if($("#"+selectId).get(0).options[i].text == selectValue)
        {
            $("#"+selectId).get(0).options[i].selected = true;
            break;
        }
    }
}

//得到地区码，后期如果将省市区存本地数据库，可以只存储地区码，会用到下面的函数
function getAreaID(){
    var area = 0;
    if($("#seachdistrict").val() != "0"){
        area = $("#seachdistrict").val();
    }else if ($("#seachcity").val() != "0"){
        area = $("#seachcity").val();
    }else{
        area = $("#seachprov").val();
    }
    return area;
}

function showAreaID() {
    //地区码
    var areaID = getAreaID();
    //地区名
    var areaName = getAreaNamebyID(areaID) ;
    alert("您选择的地区码：" + areaID + "      地区名：" + areaName);
}

//根据地区码查询地区名
function getAreaNamebyID(areaID){
    var areaName = "";
    if(areaID.length == 2){
        areaName = area_array[areaID];
    }else if(areaID.length == 4){
        var index1 = areaID.substring(0, 2);
        areaName = area_array[index1] + " " + sub_array[index1][areaID];
    }else if(areaID.length == 6){
        var index1 = areaID.substring(0, 2);
        var index2 = areaID.substring(0, 4);
        areaName = area_array[index1] + " " + sub_array[index1][index2] + " " + sub_arr[index2][areaID];
    }
    return areaName;
}

