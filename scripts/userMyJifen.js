/**
 * Created by Administrator on 2016/7/12 0012.
 */
$(document).ready(function(){
    $.ajax({
        url:server+"/smit/getIcApplyPoint",
        type: "POST",
        dataType:'json',
        data: {"hotelCode":"capital","icnum":"391920"},
        timeout: 5000,
        beforeSend: function(){
            //jsonp 方式此方法不被触发.原因可能是dataType如果指定为jsonp的话,就已经不是ajax事件了
            weui.Loading.show('处理中');
        },
        success: function (json) {//客户端jquery预先定义好的callback函数,成功获取跨域服务器上的json数据后,会动态执行这个callback函数
            if(json.bizData.success){
                var rows=json.bizData.bizData.data;
                console.log(rows);
                for (var i=0;i<rows.length;i++){
                    var hotelRoomVO=rows[i];""
                    var html="<li><p>"+hotelRoomVO.applydrpt+"</p><p>"+hotelRoomVO.applyDate
                        +"</p><span>_<big>"+hotelRoomVO.score+"</big></span></li>";

                    $("#jifenJilu").prepend(html);
                }

            }else{
                //  alert(json.bizData.msg);
                alert("查看历史记录");
            }
        },
        complete: function(XMLHttpRequest, textStatus){
            weui.Loading.hide();
        },
        error: function(xhr){
            //jsonp 方式此方法不被触发.原因可能是dataType如果指定为jsonp的话,就已经不是ajax事件了
            //请求出错处理
            weui.alert("请求出错(请检查相关度网络状况.)");
        }
    });
})
$(function () {
    if(appStorage.getItem("usertoken")==undefined){
        window.location.href="login.html";
    }else{
        $("#div_noLogin").hide();
        var userinfo = JSON.parse(appStorage.getItem('usertoken'));
        console.log(userinfo);
        console.log($("#div_okLogin span"));
        $("#div_okLogin span").get(0).innerText=userinfo.name;
        $("#div_okLogin span").get(1).innerText=userinfo.desc;
        $("#jifen").html(userinfo.score);
        $("#div_okLogin").show();

    }
});