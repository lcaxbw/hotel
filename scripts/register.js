/**
 * Created by Administrator on 2016/6/24.
 */
var clock = '';
var nums = 60;
var btn;
function sendCode(thisBtn)
{
    btn = thisBtn;
    var phone=$("#phone").val();
    var  re = /^1\d{10}$/
    if(!re.test(phone)){
        weui.alert("手机号码不正确，请重新输入");
        return false;
    }
        $.ajax({
            async:false,
            url: server+"/system/member/getVerifyCode?phone="+phone,
            type: "GET",
            dataType:'json',
            timeout: 5000,
            success: function (json) {//客户端jquery预先定义好的callback函数,成功获取跨域服务器上的json数据后,会动态执行这个callback函数
                console.log(json);
                var bizData = json.bizData;
                if(bizData.success){
                    weui.alert("验证码已发送至您的手机请注意查收");
                }else{
                    weui.alert(bizData.msg);
                }
            },
            complete: function(XMLHttpRequest, textStatus){
            },
            error: function(xhr){
                //jsonp 方式此方法不被触发.原因可能是dataType如果指定为jsonp的话,就已经不是ajax事件了
                //请求出错处理
                weui.alert("发送失败，请重新发送");


            }
        });
        btn.value = nums+'秒后可重新获取';
        clock = setInterval(doLoop, 1000); //一秒执行一次

}
function doLoop()
{ nums--;
    if(nums > 0){
        btn.value = nums+'秒后可重新获取';
    }else{
        clearInterval(clock); //清除js定时器
        btn.disabled = false;
        btn.value = '点击发送验证码';
        nums = 60; //重置时间
    }
}
$(function () {
    //todo
   /* $.getJSON("http://192.168.0.102:8080/system/member/test",
        function(json){
           alert(json);
    });*/
    $("#btnRegister").click(function () {

        //检查表单
        if($("#frmRegister input[name='phone']").val()==''){
            weui.alert("手机号码不能为空");
            return false;
        }
        if($("#frmRegister input[name='verifyCode']").val()==''){
            weui.alert("验证码不能为空");
            return false;
        }
        if($("#frmRegister input[name='password']").val()==''){
            weui.alert("密码不能为空");
            return false;
        }
    //    alert($("input[type=radio]").attr("class"));
        console.log($("#frmRegister input[name='regxy']"));
        if(!$("input[name='regxy']").prop('checked')){
            weui.alert("必须同意注册协议");
            return false;
        }
        var formDataArray=$("#frmRegister").serializeArray();
        console.log(formDataArray);
        $.ajax({
            url: server+"/system/member/register",
            type: "POST",
            dataType:'json',
            data: formDataArray,
            timeout: 50000,
            beforeSend: function(){
                //jsonp 方式此方法不被触发.原因可能是dataType如果指定为jsonp的话,就已经不是ajax事件了
                weui.Loading.show('处理中');
            },
            success: function (json) {//客户端jquery预先定义好的callback函数,成功获取跨域服务器上的json数据后,会动态执行这个callback函数
                if(json.bizData.success){
                    window.location.href="index.html";
                }else{
                  //  alert(json.bizData.msg);
                    alert("注册失败");
                }
            },
            complete: function(XMLHttpRequest, textStatus){
                weui.Loading.hide();
            },
            error: function(xhr){
                //jsonp 方式此方法不被触发.原因可能是dataType如果指定为jsonp的话,就已经不是ajax事件了
                //请求出错处理
                weui.alert("请求出错(请检查相关度网络状况.)");
            }
        });
    });

});