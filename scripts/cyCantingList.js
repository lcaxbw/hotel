/**
 * Created by Administrator on 2016/7/18 0018.
 */
$(function () {
    if(appStorage.getItem("usertoken")!=undefined){
        $("#div_noLogin").hide();
        var userinfo = JSON.parse(appStorage.getItem('usertoken'));
        $("#div_okLogin span").get(0).innerText=userinfo.name;
        $("#div_okLogin span").get(1).innerText=userinfo.desc;
        $("#div_okLogin").show();
    }
});