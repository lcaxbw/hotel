/**
 * Created by Administrator on 2016/7/14 0014.
 */
var clock = '';
var nums = 60;
var btn;
function sendCodes(thisBtn)
{
    btn = thisBtn;
    btn.disabled = true; //将按钮置为不可点击
    var phone=$("#phoneId").val();
    console.log("手机号码");
    console.log(phone);
    if(phone!=null&&phone!=""){
        $.ajax({
            async:false,
            url: server+"/system/member/getVerifyCode?phone="+phone,
            type: "GET",
            dataType:'json',
            timeout: 5000,
            success: function (json) {//客户端jquery预先定义好的callback函数,成功获取跨域服务器上的json数据后,会动态执行这个callback函数
                console.log(json);
                var bizData = json.bizData;
                if(bizData.success){
                    weui.alert("验证码已发送至您的手机请注意查收");
                }else{
                    weui.alert(bizData.msg);
                }

            },
            complete: function(XMLHttpRequest, textStatus){
            },
            error: function(xhr){
                //jsonp 方式此方法不被触发.原因可能是dataType如果指定为jsonp的话,就已经不是ajax事件了
                //请求出错处理
                weui.alert("发送失败，请重新发送");
            }
        });
        btn.value = nums+'秒后可重新获取';
        clock = setInterval(doLoop, 1000); //一秒执行一次
    }
}
function doLoop()
{ nums--;
    if(nums > 0){
        btn.value = nums+'秒后可重新获取';
    }else{
        clearInterval(clock); //清除js定时器
        btn.disabled = false;
        btn.value = '点击发送验证码';
        nums = 60; //重置时间
    }
}

$(function () {
    //todo
    /* $.getJSON("http://192.168.0.102:8080/system/member/test",
     function(json){
     alert(json);
     });*/
    $("#wangjimima").click(function () {
       // console.log($("#frmRegister input[name='phone']").val());
        //检查表单
        var phone=$("#phoneId").val();
        var verifyCode=$("#verifyCodeId").val();
        var password=$("#passwordId").val();

        if($("#phoneId").val()==''){
            weui.alert("手机号码不能为空");
            return false;
        }
        if($("#verifyCodeId").val()==''){
            weui.alert("验证码不能为空");
            return false;
        }
        if($("#passwordId").val()==''){
            weui.alert("密码不能为空");
            return false;
        }
        var formDataArray=$("#frmRegister").serializeArray();
      //  console.log("form=======");
      //  console.log(formDataArray);
        $.ajax({
            url: server+"/system/member/register",
            type: "POST",
            dataType:'json',
            data: {"phone":phone,"verifyCode":verifyCode,"password":password},
            timeout: 5000,
            beforeSend: function(){
                //jsonp 方式此方法不被触发.原因可能是dataType如果指定为jsonp的话,就已经不是ajax事件了
                weui.Loading.show('处理中');
            },
            success: function (json) {//客户端jquery预先定义好的callback函数,成功获取跨域服务器上的json数据后,会动态执行这个callback函数
                if(json.bizData.success){
                    window.location.href="success.html";
                }else{
                    //  alert(json.bizData.msg);
                    alert("注册失败");
                }
            },
            complete: function(XMLHttpRequest, textStatus){
                weui.Loading.hide();
            },
            error: function(xhr){
                //jsonp 方式此方法不被触发.原因可能是dataType如果指定为jsonp的话,就已经不是ajax事件了
                //请求出错处理
                weui.alert("请求出错(请检查相关度网络状况.)");
            }
        });
    });

});

$(function () {
    if(appStorage.getItem("usertoken")==undefined){
        window.location.href="login.html";
    }else{
        $("#div_noLogin").hide();
        var userinfo = JSON.parse(appStorage.getItem('usertoken'));
        $("#div_okLogin span").get(0).innerText=userinfo.name;
        $("#div_okLogin span").get(1).innerText=userinfo.desc;
        $("#div_okLogin").show();

    }
});